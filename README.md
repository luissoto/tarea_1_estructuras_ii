# Tarea 1 - Estructuras II

Este repositorio contiene el código de la implementación de cuatro predictores de saltos, uno Bimodal, otro de con un registro de historia global, otro con una tabla de historia privada, y un último Metapredictor.

Al ejecutarse, se simula el comportamiento de uno de estos cuatro predictores ante 16 millones de entradas de saltos tomados, y se imprimen la dirección de pc, el resultado y la predicción de los primeros 5000 saltos, así como si este se predijo de forma correcta.

Instrucciones:

Para la ejecución del programa se debe tener un compilador de lenguaje c++, se debe abrir un terminal en el mismo directorio donde se encuentra el archivo Tarea1.cpp y branch-trace-gcc.trace.gz, y usar la siguiente instrucción:

$ g++ Tarea1.cpp -o <Nombre de ejecutable>

Con esto se crea un binaro ejecutable, para la siguiente instrucción consideraremos que se eligió T1 para el nombre de este archivo, y se procede a ejecutar el siguiente comando:

$ gunzip -c branch-trace-gcc.trace.gz | ./T1 -s <#> -bp <#> -gh <#> -ph <#> -o <Archivo de texto de salida>

Los argumentos que se envían al ejecutable representan lo siguiente:

-s: Bits del tamaño en las BHTs
-bp: Tipo de predictor a usarse, en mi caso se seleccionó que estas fueran:

    1:  Bimodal
    2:  Gshare
    3:  Pshare
    4:  Metapredictor

-gh: Bits del registro de historia global
-ph: Bits de los registros de historia privada
-o: Nombre del archivo de salida donde se guardaran el resultado de las primeras 5000 predicciones.
