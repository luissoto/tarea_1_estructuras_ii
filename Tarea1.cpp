#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <math.h> 
#include<string>

using namespace std;

// argc: la cantidad de argumentos enviados 
// argv: los valores de los argument enviados
int main(int argc, char** argv) {

    //Variables donde se guardaran los argumentos de entrada
    int BHTSize;
    int PHTSize;
    int predicType; 
    int GHSize;
    int PHSize;
    string salida;
    string options[5] = {"-s","-bp","-gh","-ph","-o"};

    //Recorre los argumentos de entrada
    for (int i = 0; i < 11; i++)
    {
        //Si el argumento es -s guarda el siguiente valor en BTHSize
        if (argv[i] == options[0])
        {
            BHTSize = atoi(argv[i+1]);
            PHTSize = atoi(argv[i+1]);
        }

        //Si el argumento es -bp guarda el siguiente valor en predicType
        if (argv[i] == options[1])
        {
            predicType = atoi(argv[i+1]);
        }
        
        //Si el argumento es -gh guarda el siguiente valor en GHSize
        if (argv[i] == options[2])
        {
            GHSize = atoi(argv[i+1]);
        }

        //Si el argumento es -ph guarda el siguiente valor en PHSize
        if (argv[i] == options[3])
        {
            PHSize = atoi(argv[i+1]);
        }

        //Si el argumento es -o guarda el siguiente string en salida
        if (argv[i] == options[4])
        {
            salida = argv[i+1];
        }
    }
    
    //Optiene el total de entradas para BHT y PHT
    int BHTentries = pow(2,BHTSize);
    int PHTentries = pow(2,PHTSize);
    

    ///Predictores

    unsigned int BHT[BHTentries]; //Branch history table
    unsigned int BHTP[BHTentries];  //BHTs para el tournament
    unsigned int BHTG[BHTentries]; 
    unsigned int PHT[PHTentries];   //Private histoy table
    unsigned int TOURNAMENT[PHTentries]; //Tabla de seleción del meta prdictor
    unsigned int GH = 0; //Global history

    //Para los estados se tendrán:
    //00 = Strong not taken
    //01 = Weak not taken
    //10 = Weak taken
    //11 = Strong taken

    //Se recorre la tabla y se inicializa todos
    //los contadores en Strong not taken
    for (int i = 0; i < BHTentries; i++)
    {
        BHT[i] = 0;
    }

    for (int i = 0; i < PHTentries; i++)
    {
        PHT[i] = 0;
    }

    for (int i = 0; i < PHTentries; i++)
    {
        TOURNAMENT[i] = 0;
    }

    for (int i = 0; i < BHTentries; i++)
    {
        BHTP[i] = 0;
    }
    for (int i = 0; i < BHTentries; i++)
    {
        BHTG[i] = 0;
    }

    //Se inicializan los valores de recepción de datos, la dirección del salto,
    //los punteros con los bits del salto, el resultado de este y la predicción que se tiene
    
    string datos;
    unsigned int addr; //PC salto
    unsigned int pntr;  
    unsigned int pntrb; //Puntero para BHT
    unsigned int pntrp; //Puntero con PHT
    unsigned int pntrg; //Puntero con GH
    string result;      //resultado del branch
    string prediction;  //Predicción del branch
    string predictionP; //Predicción del ph
    string predictionG; //predicción del gh
    string predictor;   //predicción del gh
    int prefer;         //preferencia de predictor (o pshare, 1 gshare)

    //A continuación, se inicializan las variables con datos sobre la simulación
    //estas son las que se muestran en la tabla final

    int branches = 0;
    int correctTaken = 0;
    int correctNotTaken = 0;
    int incorrectTaken = 0;
    int incorrectNotTaken = 0;
    double correctPercentage = 0.0;

    //Se crea un archivo de salida llamado como el argumento ingresado para este
    ofstream outfile (salida.c_str());

    outfile << "PC          Outcome  Prediction  Correct/Incorrect\n";

    //Para la selección de predictor se usa un switch case
    switch (predicType)
    {

    //Caso 1: Predictor Binomial    
    case 1:
        {
            //Se tiene un for que itera por cada dato recibido
            for (int i = 0; i < 16000000; i++)
            {
                //Se suma 1 al contador de branches
                branches++;

                //Se recibe el dato de entrada
                cin >> datos;
                addr = strtol(datos.c_str(), NULL, 10);
                pntr = addr<<(32-BHTSize);
                pntr = pntr>>(32-BHTSize);                 

                cin >> datos;
                result = datos.c_str();
                
                //Si el contador está en 00 o 01, la predicción es Not taken
                if (BHT[pntr] == 0 || BHT[pntr] == 1) {
                    prediction = "N";
                }
                //Si el contador está en 10 o 11, la predicción es Taken
                if (BHT[pntr] == 2 || BHT[pntr] == 3) {
                    prediction = "T";
                }
                
                //Si la predicción de N fue correcta, se resta 1 al contador (en caso de no ser SN)
                if (result == prediction && result == "N") {
                    if (BHT[pntr] > 0) {
                        BHT[pntr]--;
                    }
                    correctNotTaken++;
                    if (branches < 5000){
                        outfile << addr << "  " << result << "        " << prediction << "           Correct\n";
                    }          
                }

                //Si la predicción de T fue correcta, se suma 1 al contador (en caso de no ser ST)
                if (result == prediction && result == "T") {
                    if (BHT[pntr] < 3) {
                        BHT[pntr]++;
                    }         
                    correctTaken++;
                    if (branches < 5000){
                        outfile << addr << "  " << result << "        " << prediction << "           Correct\n";
                    }
                }

                //Si la predicción de N fue incorrecta, se resta 1 al contador (en caso de no ser SN)
                if (result != prediction && result == "N") {
                    if (BHT[pntr] > 0) {
                        BHT[pntr]--;
                    }         
                    incorrectNotTaken++;
                    if (branches < 5000){
                    outfile << addr << "  " << result << "        " << prediction << "           Incorrect\n";
                    }
                }

                //Si la predicción de T fue incorrecta, se suma 1 al contador (en caso de no ser ST)
                if (result != prediction && result == "T") {
                    if (BHT[pntr] < 3) {
                        BHT[pntr]++;
                    }         
                    incorrectTaken++;
                    if (branches < 5000){
                        outfile << addr << "  " << result << "        " << prediction << "           Incorrect\n";
                    }
                }
            }
        //Se calcula el porcentaje de aciertos
        correctPercentage = (double) (100*(correctTaken+correctNotTaken))/branches;
        }
        break;

    //Caso 2: Predictor Global
    case 2:
        {
            //Se tiene un for que itera por cada dato recibido
            for (int i = 0; i < 16000000; i++)
            {
                //Se suma 1 al contador de branches
                branches++;

                //Se recibe la dir de branch
                cin >> datos;
                addr = strtol(datos.c_str(), NULL, 10);

                //Se calcula el puntero del XOR
                pntr = addr^GH;
                pntr = pntr<<(32-BHTSize);
                pntr = pntr>>(32-BHTSize);                 

                //Se recibe el resultado
                cin >> datos;
                result = datos.c_str();
                
                //Si el contador está en 00 o 01, la predicción es Not taken
                if (BHT[pntr] == 0 || BHT[pntr] == 1) {
                    prediction = "N";
                }

                //Si el contador está en 10 o 11, la predicción es Taken
                if (BHT[pntr] == 2 || BHT[pntr] == 3) {
                    prediction = "T";
                }
                
                //Si la predicción de N fue correcta, se resta 1 al contador (en caso de no ser SN)
                if (result == prediction && result == "N") {
                    if (BHT[pntr] > 0) {
                        BHT[pntr]--;
                    }
                    correctNotTaken++;
                    GH = GH<<1;
                    GH = GH<<(32-GHSize);
                    GH = GH>>(32-GHSize);
                    if (branches < 5000){
                        outfile << addr << "  " << result << "        " << prediction << "           Correct\n";          
                    }
                }

                //Si la predicción de T fue correcta, se suma 1 al contador (en caso de no ser ST)
                if (result == prediction && result == "T") {
                    if (BHT[pntr] < 3) {
                        BHT[pntr]++;
                    }         
                    correctTaken++;
                    GH = GH<<1;
                    GH++;
                    GH = GH<<(32-GHSize);
                    GH = GH>>(32-GHSize);
                    if (branches < 5000){
                        outfile << addr << "  " << result << "        " << prediction << "           Correct\n";
                    }
                }

                //Si la predicción de N fue incorrecta, se resta 1 al contador (en caso de no ser SN)
                if (result != prediction && result == "N") {
                    if (BHT[pntr] > 0) {
                        BHT[pntr]--;
                    }         
                    incorrectNotTaken++;
                    GH = GH<<1;
                    GH = GH<<(32-GHSize);
                    GH = GH>>(32-GHSize);
                    if (branches < 5000){
                        outfile << addr << "  " << result << "        " << prediction << "           Incorrect\n";
                    }
                }

                //Si la predicción de T fue incorrecta, se suma 1 al contador (en caso de no ser ST)
                if (result != prediction && result == "T") {
                    if (BHT[pntr] < 3) {
                        BHT[pntr]++;
                    }         
                    incorrectTaken++;
                    GH = GH<<1;
                    GH++;
                    GH = GH<<(32-GHSize);
                    GH = GH>>(32-GHSize);
                    if (branches < 5000){
                        outfile << addr << "  " << result << "        " << prediction << "           Incorrect\n";
                    }
                }
            }

        correctPercentage = (double) (100*(correctTaken+correctNotTaken))/branches;
        }
        break;
    //Caso 3: Predictor Privado
    case 3:
        {
            //Se tiene un for que itera por cada dato recibido
            for (int i = 0; i < 16000000; i++)
            {
                //Se suma 1 al contador de branches
                branches++;

                //Se recibe el dato de entrada
                cin >> datos;
                addr = strtol(datos.c_str(), NULL, 10);

                //Se calcula el puntero del XOR
                pntr = addr<<(32-PHTSize);
                pntr = pntr>>(32-PHTSize);
                pntrb = PHT[pntr]^pntr;
                pntrb = pntrb<<(32-BHTSize);
                pntrb = pntrb>>(32-BHTSize);            

                //Se recibe el resultado
                cin >> datos;
                result = datos.c_str();

                //Si el contador está en 00 o 01, la predicción es Not taken
                if (BHT[pntrb] == 0 || BHT[pntrb] == 1) {
                    prediction = "N";
                }

                //Si el contador está en 10 o 11, la predicción es Taken
                if (BHT[pntrb] == 2 || BHT[pntrb] == 3) {
                    prediction = "T";
                }
                
                //Si la predicción de N fue correcta, se resta 1 al contador (en caso de no ser SN)
                if (result == prediction && result == "N") {
                    if (BHT[pntrb] > 0) {
                        BHT[pntrb]--;
                    }
                    correctNotTaken++;
                    PHT[pntr] = PHT[pntr]<<1;
                    PHT[pntr] = PHT[pntr]<<(32-PHSize);
                    PHT[pntr] = PHT[pntr]>>(32-PHSize);
                    if (branches < 5000){
                        outfile << addr << "  " << result << "        " << prediction << "           Correct\n";          
                    }
                }

                //Si la predicción de T fue correcta, se suma 1 al contador (en caso de no ser ST)
                if (result == prediction && result == "T") {
                    if (BHT[pntrb] < 3) {
                        BHT[pntrb]++;
                    }         
                    correctTaken++;
                    PHT[pntr] = PHT[pntr]<<1;
                    PHT[pntr]++;
                    PHT[pntr] = PHT[pntr]<<(32-PHSize);
                    PHT[pntr] = PHT[pntr]>>(32-PHSize);
                    if (branches < 5000){
                        outfile << addr << "  " << result << "        " << prediction << "           Correct\n";
                    }
                }

                //Si la predicción de N fue incorrecta, se resta 1 al contador (en caso de no ser SN)
                if (result != prediction && result == "N") {
                    if (BHT[pntrb] > 0) {
                        BHT[pntrb]--;
                    }         
                    incorrectNotTaken++;
                    PHT[pntr] = PHT[pntr]<<1;
                    PHT[pntr] = PHT[pntr]<<(32-PHSize);
                    PHT[pntr] = PHT[pntr]>>(32-PHSize);
                    if (branches < 5000){
                        outfile << addr << "  " << result << "        " << prediction << "           Incorrect\n";
                    }
                }

                //Si la predicción de T fue incorrecta, se suma 1 al contador (en caso de no ser ST)
                if (result != prediction && result == "T") {
                    if (BHT[pntrb] < 3) {
                        BHT[pntrb]++;
                    }         
                    incorrectTaken++;
                    PHT[pntr] = PHT[pntr]<<1;
                    PHT[pntr]++;
                    PHT[pntr] = PHT[pntr]<<(32-PHSize);
                    PHT[pntr] = PHT[pntr]>>(32-PHSize);
                    if (branches < 5000){
                        outfile << addr << "  " << result << "        " << prediction << "           Incorrect\n";
                    }                  
                }
            }

        //Se calcula el porcentaje de aciertos
        correctPercentage = (double) (100*(correctTaken+correctNotTaken))/branches;
        }
        break;

    //Caso 4: Predictor por Torneo
    case 4:
        {
            for (int i = 0; i < 16000000; i++)
            {
                //Se suma 1 al contador de branches
                branches++;

                //Se recibe el dato de entrada
                cin >> datos;
                addr = strtol(datos.c_str(), NULL, 10);

                pntr = addr<<(32-PHTSize);      //pntr contiene la cantidad de bits útiles del addres
                pntr = pntr>>(32-PHTSize);      
                
                pntrp = PHT[pntr]^pntr;     
                pntrp = pntrp<<(32-BHTSize);
                pntrp = pntrp>>(32-BHTSize);    //pntrb contiene los bits útiles del XOR entre la historia privada y el pc

                pntrg = addr^GH;
                pntrg = pntrg<<(32-BHTSize);
                pntrg = pntrg>>(32-BHTSize);     //pntrg contiene los bits útiles del XOR entre la historia global y el pc

                //Se recibe los resultados
                cin >> datos;
                result = datos.c_str();
                
                //Se obtiene la predicción del Pshared
                if (BHTP[pntrp] == 0 || BHTP[pntrp] == 1) {
                    predictionP = "N";
                }
                if (BHTP[pntrp] == 2 || BHTP[pntrp] == 3) {
                    predictionP = "T";
                }

                //Se obtiene la predicción del Gshared
                if (BHTG[pntrg] == 0 || BHTG[pntrg] == 1) {
                    predictionG = "N";
                }
                if (BHTG[pntrg] == 2 || BHTG[pntrg] == 3) {
                    predictionG = "T";
                }  

                //Se obtiene cual predictor prefiere el selecionador por torneo
                if (TOURNAMENT[pntr] == 0 || TOURNAMENT[pntr] == 1) { //Prefer PShared
                    prefer = 0;
                    prediction = predictionP;
                    predictor = "P";
                }
                if (TOURNAMENT[pntr] == 2 || TOURNAMENT[pntr] == 3) { //Prefer GShared
                    prefer = 1;
                    prediction = predictionG;
                    predictor = "G";
                }

                //Predicción correcta de N
                if (result == prediction && result == "N") {
                    //Se actualiza la tabla de BHTs
                    if (BHTP[pntrp] > 0) {
                        BHTP[pntrp]--;
                    }
                    if (BHTG[pntrg] > 0) {
                        BHTG[pntrg]--;
                    }

                    correctNotTaken++;
                    PHT[pntr] = PHT[pntr]<<1;               //Se actualiza el contador de historia privada
                    PHT[pntr] = PHT[pntr]<<(32-PHSize);
                    PHT[pntr] = PHT[pntr]>>(32-PHSize);
                    GH = GH<<1;                             //Se actualiza el contador de historia global
                    GH = GH<<(32-GHSize);
                    GH = GH>>(32-GHSize);

                    //Se actualiza la tabla de preferencia
                    if(predictionP != predictionG) {
                        if (prefer == 0 && TOURNAMENT[pntr] > 0) {
                            TOURNAMENT[pntr]--;
                        }
                        if (prefer == 1 && TOURNAMENT[pntr] < 3) {
                            TOURNAMENT[pntr]++;
                        } 
                    }
                    if (branches < 5000){
                        outfile << addr << "  " << predictor << "  " << result << "        " << prediction << "           Correct\n";          
                    }
                }

                //Predicción correcta de T
                if (result == prediction && result == "T") {
                    if (BHTP[pntrp] < 3) {
                        BHTP[pntrp]++;
                    }
                    if (BHTG[pntrg] < 3) {
                        BHTG[pntrg]++;
                    }
                    correctTaken++;
                    PHT[pntr] = PHT[pntr]<<1;               //Se actualiza el contador de historia privada
                    PHT[pntr]++;
                    PHT[pntr] = PHT[pntr]<<(32-PHSize);
                    PHT[pntr] = PHT[pntr]>>(32-PHSize);
                    GH = GH<<1;                             //Se actualiza el contador de historia global
                    GH++;
                    GH = GH<<(32-GHSize);
                    GH = GH>>(32-GHSize);

                    //Se actualiza la tabla de preferencia
                    if(predictionP != predictionG) {
                        if (prefer == 0 && TOURNAMENT[pntr] > 0) {
                            TOURNAMENT[pntr]--;
                        }
                        if (prefer == 1 && TOURNAMENT[pntr] < 3) {
                            TOURNAMENT[pntr]++;
                        } 
                    }
                    if (branches < 5000){
                        outfile << addr << "  " << predictor << "  " << result << "        " << prediction << "           Correct\n";          
                    }
                }


                //Predicción incorrecta de N
                if (result != prediction && result == "N") {
                    //Se actualiza los BHTs
                    if (BHTP[pntrp] > 0) {
                        BHTP[pntrp]--;
                    }
                    if (BHTG[pntrg] > 0) {
                        BHTG[pntrg]--;
                    }

                    incorrectNotTaken++;
                    PHT[pntr] = PHT[pntr]<<1;               //Se actualiza el contador de historia privada
                    PHT[pntr] = PHT[pntr]<<(32-PHSize);
                    PHT[pntr] = PHT[pntr]>>(32-PHSize);
                    GH = GH<<1;                             //Se actualiza el contador de historia global
                    GH = GH<<(32-GHSize);
                    GH = GH>>(32-GHSize);

                    //Se actualiza la tabla de preferencia
                    if(predictionP != predictionG) {
                        if (prefer == 0 && TOURNAMENT[pntr] < 3) {
                            TOURNAMENT[pntr]++;
                        }
                        if (prefer == 1 && TOURNAMENT[pntr] > 0) {
                            TOURNAMENT[pntr]--;
                        } 
                    }

                    if (branches < 5000){
                        outfile << addr << "  " << predictor << "  " << result << "        " << prediction << "           Incorrect\n";          
                    }
                }

                //Predicción incorrecta de T
                if (result != prediction && result == "T") {
                    //Se actualiza los BHTs
                    if (BHTP[pntrp] < 3) {
                        BHTP[pntrp]++;
                    }
                    if (BHTG[pntrg] < 3) {
                        BHTG[pntrg]++;
                    }

                    incorrectTaken++;
                    PHT[pntr] = PHT[pntr]<<1;               //Se actualiza el contador de historia privada
                    PHT[pntr]++;
                    PHT[pntr] = PHT[pntr]<<(32-PHSize);
                    PHT[pntr] = PHT[pntr]>>(32-PHSize);
                    GH = GH<<1;                             //Se actualiza el contador de historia global
                    GH++;
                    GH = GH<<(32-GHSize);
                    GH = GH>>(32-GHSize);

                    //Se actualiza la tabla de preferencias
                    if(predictionP != predictionG) {
                        if (prefer == 0 && TOURNAMENT[pntr] < 3) {
                            TOURNAMENT[pntr]++;
                        }
                        if (prefer == 1 && TOURNAMENT[pntr] > 0) {
                            TOURNAMENT[pntr]--;
                        } 
                    }

                    if (branches < 5000){
                        outfile << addr << "  " << predictor << "  " << result << "        " << prediction << "           Incorrect\n";          
                    }
                }    
    
            }
            //Se calcula el porcentaje de aciertos
        correctPercentage = (double) (100*(correctTaken+correctNotTaken))/branches;
        }
        break;
    default: {}
    }
    
    //Print on Console
    cout << "---------------------------------------------------------------\n";
    cout << "Prediction Parameter:\n";
    cout << "---------------------------------------------------------------\n";

    //Se imprime el tipo de predictor a usar
    if (predicType == 1)
    {
        cout << "Branch prediction type:                                 Bimodal\n";
    }
    if (predicType == 2)
    {
        cout << "Branch prediction type:                          Global History\n";
    }
    if (predicType == 3)
    {
        cout << "Branch prediction type:                         Private History\n";
    }
    if (predicType == 4)
    {
        cout << "Branch prediction type:                              Tournament\n";
    }
    
    //Y se imprime el resto de valores obtenidos en la simulación
    cout << "BHT size:                                                 " << BHTentries << "\n";
    cout << "Global history register size:                             " << GHSize << "\n";
    cout << "Private history register size:                            " << PHSize << "\n";

    cout << "---------------------------------------------------------------\n";
    cout <<"Simulation Results:\n";
    cout << "---------------------------------------------------------------\n";
    cout << "Number of branch:                                         " << branches << "\n";
    cout << "Number of correct prediction of taken branches:           " << correctTaken << "\n";
    cout << "Number of incorrect prediction of taken branches:         " << incorrectTaken << "\n";
    cout << "Number of correct prediction of not taken branches:       " << correctNotTaken << "\n";
    cout << "Number of incorrect prediction of not taken branches:     " << incorrectNotTaken << "\n";
    cout << "Percentage of correct predictions:                        " << correctPercentage << "\n";
    cout << "---------------------------------------------------------------\n";

}